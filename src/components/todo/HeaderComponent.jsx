import React , {Component} from 'react'
import {BrowserRouter as  Link} from 'react-router-dom'
import AuthService  from './AuthService.jsx'
//import AuthenticatedRoute from './AuthenticatedRoute.jsx'
import { withRouter } from 'react-router';

class HeaderComponent extends Component{
    render (){
        const isUserLoggedIn = AuthService.isUserLoggedIn()
       // console.log(isUserLoggedIn)
        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark"> 
                    <div><a  href= "https://www.linkedin.com/in/narjess-hmida-639439165/" className="navbar-brand" > Narjess </a></div>
                    <ul className="navbar-nav "> 
                        {isUserLoggedIn && <li><Link className= "nav-link" to ="/welcome/narjess">Home</Link></li>}
                        {isUserLoggedIn && <li><Link className= "nav-link" to ="/todos">Todos</Link> </li>}
                        </ul>
                        <ul className="navbar-nav navbar-collapse justify-content-end">
                        {!isUserLoggedIn &&<li> <Link className= "nav-link" to ="/login">Log in </Link> </li>}
                        {isUserLoggedIn &&<li> <Link className= "nav-link" to ="/logout" onClick = {AuthService.logout}>Log out</Link></li>}
                        </ul>
                    </nav> 
            </header>
            
        )
    }
}

export default withRouter(HeaderComponent)