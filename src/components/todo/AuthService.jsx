
import axios from 'axios'

class AuthService{

    executeBasicAuthService(username, password){

         let BasicAuthHeader = 'Basic ' +  window.btoa(username + ":"  + password)
        return axios.get(`http://localhost:8080/basicauth`), {
              headers : {
                      authorization : BasicAuthHeader }
          }
    }

    createBasicAuthToken(username, password){
        let BasicAuthHeader = 'Basic ' +  window.btoa(username + ":"  + password)


    }

    registerSuccessfullLogin(username,password){
       
        let BasicAuthHeader = 'Basic ' +  window.btoa(username + ":"  + password)
    //   console.log('registerSuccessfullLogin')
    sessionStorage.setItem('authenticatedUser', username)
    this.setupAxiosInterceptors( BasicAuthHeader)

}

    logout(){

        sessionStorage.removeItem('authenticatedUser');
}

isUserLoggedIn(){
    let user = sessionStorage.getItem('authenticatedUser')
    if(user===null) return false
    return true
}

GetLoggedInUserName(){
    let user = sessionStorage.getItem('authenticatedUser')
    if(user===null) return ''
    return user
}


setupAxiosInterceptors( BasicAuthHeader){
    

    axios.interceptors.request.use(
        (config) => {
            if (this.isUserLoggedIn()){
            config.headers.authorization = BasicAuthHeader
            }
            return config

        }
    )

}


}
export default new AuthService()