
import React, {Component} from 'react'
//import {Route, Redirect} from 'react-router-dom'
import AuthService from './AuthService.jsx'
import TodoDataService from '../../api/todo/TodoDataService.js'
import moment from 'moment'

class ListTodosComponent extends Component {

    constructor(props){
        console.log('constructor')
        super(props)
        this.state = {
            todos :[],
            message : null
        }
        this.deleteTodoClicked=this.deleteTodoClicked.bind(this)
        this.refreshTodos=this.refreshTodos.bind(this)
        this.UpdateTodoClicked= this.UpdateTodoClicked.bind(this)
        this.addTodoClicked= this.addTodoClicked.bind(this)


    }


    componentWillUnmount(){
        console.log('componentWillUnmount')
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log('componentWillUnmount')
        console.log(nextProps)
        console.log(nextState)
        return true
    }




    componentDidMount(){
        console.log('componentDidMount')
        this.refreshTodos()

        console.log(this.state)
    }

    refreshTodos(){
        let username = AuthService.GetLoggedInUserName()
        TodoDataService.retreiveAllTodos(username)
        .then(
        response => {
            this.setState({todos : response.data})
}

        )

    }


    deleteTodoClicked(id){
        let username = AuthService.GetLoggedInUserName()
        //console.log( id + "" + username);
        TodoDataService.deleteTodo(username, id)
        .then (
            response => {
                this.setState({message : `Delete of todo ${id} sucess` } )
                this.refreshTodos()
    })

    }

    UpdateTodoClicked(id){
       // let username = AuthService.GetLoggedInUserName()
        console.log( 'update' + id );
        this.props.history.push(`/todos/${id}`)
        //TodoDataService.deleteTodo(username, id)
       // .then (
           // response => {
              //  this.setState({message : `Delete of todo ${id} sucess` } )
              //  this.refreshTodos()
 //   })

    }
    addTodoClicked(){
        // let username = AuthService.GetLoggedInUserName()
         this.props.history.push(`/todos/-1`)
        }

    render(){
        console.log('render')
        return(
        <div>
            <h1>List Todos </h1>
            {this.state.message && <div class= "alert alert-success"> {this.state.message}</div>}
            <div className="container">
            <table className="table">
                <thead>
                    
                    <tr> 
                        <th>id</th>
                        <th> description </th>
                        <th> Target Date </th>
                        <th> Is Completed ? </th>
                        <th> Update </th>
                        <th> Delete </th>
                         </tr>
                </thead>
                <tbody>
                    {
                        this.state.todos.map (
                            todo =>
                             <tr key={todo.id.toString()}>
                               <td>{todo.id}</td>
                               <td>{todo.description}</td>
                               <td>{todo.done.toString()}</td>
                               <td>{moment(todo.targetDate).format('YYYY-MM-DD')}</td>
                               <td> <button className="btn btn-success" onClick={() => this.UpdateTodoClicked(todo.id)}  >Update </button></td>
                               <td> <button className="btn btn-warning" onClick={() => this.deleteTodoClicked(todo.id)}  >Delete </button></td>
                             </tr>
                    )
                    }

                    </tbody>
                </table>
                <div className="row">
                    <button className="btn btn-success" onClick={this.addTodoClicked}>ADD </button>
                    </div>
            </div>
            </div>
        )
    }
}


export default ListTodosComponent